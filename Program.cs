﻿using System;

namespace Class_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string someText = "Hello";
            int a = 1;
            double d = 1.2;
            char c = '1';
            Console.Write(someText);
            Console.WriteLine(a);
            Console.WriteLine(d);
            Console.WriteLine("Hello World!");
            System.Console.WriteLine("Please Enter a number: ");
            string input = Console.ReadLine();

            // Solution #1 - not so good
            System.Console.WriteLine("You entered {0} not {1}", input, 312);

            // Solution #2 - 😊
            System.Console.WriteLine($"You entered {input} not {312} 😊");
        }
    }
}
